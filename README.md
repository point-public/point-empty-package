* create empty directory

```shell
mkdir <project_name>
```

* init repository

```shell
yarn init -y
```

* update package.json project name

```shell
@pointapp/<project_name>
```

* create .gitignore and copy file content from this project

* install TS

```shell
yarn add typescript --dev
```

* install eslint

```shell
yarn add eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin --dev
```

* install rimraf for cleanup before builds

```shell
yarn add rimraf --dev
```

* init TS

```shell
npx tsc --init
```

* create .tsconfig.json and copy content from this project

* update package.json settings

```json
{
  "main": "./dist/index.js",
  "types": "./dist/index.d.ts",
  "files": [
    "dist"
  ],
  "scripts": {
    "clean": "rimraf dist",
    "ts_build": "tsc --build",
    "build": "yarn run ts_build"
  }
}
```

* create .eslintrc.js with content from this project

* create .eslintignore with content from this project

* create src/ directory with index.ts file

* to build project use

```shell
tsc --build
```

* create .npmrc with registry link (project id is on GitLab repository page under project name)

```
@pointapp:registry=https://gitlab.com/api/v4/projects/<project_id>/packages/npm/
//gitlab.com/api/v4/projects/<project_id>/packages/npm/:_authToken=${GITLAB_ACCESS_TOKEN}

ex.:
@pointapp:registry=https://gitlab.com/api/v4/projects/30679729/packages/npm/
//gitlab.com/api/v4/projects/30679729/packages/npm/:_authToken=${GITLAB_ACCESS_TOKEN}
```

* to publish project:
    * [use token from project settings -> repository -> deployment tokens with read registry/write registry -> password](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html)
    * [use access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
    * [other options](https://docs.gitlab.com/ee/user/packages/npm_registry/#use-the-gitlab-endpoint-for-npm-packages)
    * you can use variable in your bash profile instead of custom command

```shell
GITLAB_ACCESS_TOKEN=<deployment_token> npm publish

ex.:
GITLAB_ACCESS_TOKEN=biLWGNUt7eY2yjvesQob npm publish
```

* to use in external project:

```shell
echo @pointapp:registry=https://gitlab.com/api/v4/projects/30684637/packages/npm/ >> .npmrc
npm i @pointapp/point-empty-package
```
